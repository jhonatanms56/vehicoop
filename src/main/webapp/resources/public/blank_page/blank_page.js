/*<![CDATA[*/
$("#grid").kendoGrid({
    selectable: "select",
    allowCopy: true,
    groupable: true,
    sortable: true,
    pageable: {
        refresh: true,
        pageSizes: true,
        buttonCount: 5
    },
    columns: [
        {field: "productName", title: "Producto"},
        {field: "category", title: "Categoria"}
    ],
    dataSource: [
        {productName: "Tea", category: "Beverages"},
        {productName: "Coffee", category: "Beverages"},
        {productName: "Ham", category: "Food"},
        {productName: "Bread", category: "Food"}
    ]
});
/*]]>*/