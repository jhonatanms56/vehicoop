/* global kendo, UIkit, modal_view, modal_view2 */

// ******** Data Sources  (Read) ********* //
var dataSource = new kendo.data.DataSource({
    transport: {
        read: {
            url: "api/empresa",
            contentType: "application/json; charset=utf-8",
            type: "GET",
            async: false
        }
    },
    pageSize: 10,
    schema: {
        model: {
            id: "idEmpresa",
            fields: {
                idEmpresa: {type: "number"},
                nit: {type: "string"},
                nombre: {type: "string"},
                descripcion: {type: "string"},
                razonSocial: {type: "string"},
                emailPrincipal: {type: "string"},
                emailAlterno: {type: "string"},
                telefonoPrincipal: {type: "string"},
                telefonoAlterno: {type: "string"},
                sitioWeb: {type: "string"},
                logo: {type: "string"}
            }
        }
    }
});
// *** **** Data Sources End *** ***** //
console.log("data_sources", dataSource);
// ********        Grid      ********* //
var grid_empresa = $("#grid_empresa").kendoGrid({
    toolbar: [{template: kendo.template($("#template").html())}],
    //detailTemplate: kendo.template($("#detail-template").html()),
    columns: [
        {field: "nit", title: "NIT"},
        {field: "nombre", title: "Nombre"},
        //{field: "descripcion", title: "Descripción"},
        //{field: "razonSocial", title: "Razón Social"},
        {field: "emailPrincipal", title: "Email"},
        {field: "telefonoPrincipal", title: "Teléfono"},
        {
            command:
                    [
                        {
                            name: "Editar",
                            text: "",
                            imageClass: "uk-icon-edit",
                            title: "Editar",
                            click: editar
                        },
                        {
                            name: "Eliminar",
                            text: "",
                            imageClass: "uk-icon-trash",
                            title: "Eliminar",
                            click: eliminar
                        }
                    ],
            title: "&nbsp;"
        }
    ],
    dataSource: dataSource,
    selectable: "multiple row",
    sortable: true,
    reorderable: true,
    groupable: true,
    resizable: true,
    filterable: true,
    columnMenu: true,
    pageable: {
        pageSize: 10,
        pageSizes: true,
        refresh: true
    }
});
// *** ****      Grid End    *** ***** //
var dataItem = null;
// ********       Create     *** *****// 
function crear() {
    modal("create", "Crear", "primary", "Guardar");
    $.ajax({
        url: "empresa/create",
        beforeSend: function (xhr) {

        },
        success: function (data, textStatus, jqXHR) {
            $("#modal-body").html(data);
        },
        error: function (jqXHR, textStatus, errorThrown) {

        }
    });
    modal_view.show();
}
// ********        Delete    *** ***** // 
function eliminar(e) {
    e.preventDefault();
    dataItem = this.dataItem($(e.currentTarget).closest("tr"));
    modal("delete", "Eliminar", "danger", "Aceptar");
    $("#modal-body").html("<h4>Realmente desea eliminar la empresa seleccionada?</h4>");
    modal_view.show();
}
// ********     Delete All     *** ***** //
var delVarAll = new Array();
function deleteAll() {
    var grid = $('#grid_empresa').data('kendoGrid');
    console.log("tamaño: ", grid.select().length);
    if(grid.select().length>0){
        grid.select().each(function () {
            var dataItem = grid.dataItem($(this));
            delVarAll.push(dataItem.idEmpresa);
        });
        //console.log("id",delVarAll);
        //console.log("grid_empresa: ",grid.select());
        modal("delete_all", "Eliminar Selección", "danger", "Aceptar");
        $("#modal-body").html("<h4>Realmente desea eliminar las empresas seleccionadas?</h4>");
        modal_view.show();
    } else {
        UIkit.notify({
            message: "<a href='javascript:void(0);' class='notify-action'>X</a> Seleccione por lo menos una fila",
            status: 'warning',
            timeout: 5000,
            pos: 'bottom-center'
        });
    }
    
}
// ********        Update      *** ***** // 
function editar(e) {
    e.preventDefault();
    dataItem = this.dataItem($(e.currentTarget).closest("tr"));
    modal("update", "Actualizar", "primary", "Guardar");
    $.ajax({
        url: "empresa/create",
        beforeSend: function (xhr) {

        },
        success: function (data, textStatus, jqXHR) {
            $("#modal-body").html(data);
            cargar_input(dataItem);
        },
        error: function (jqXHR, textStatus, errorThrown) {

        }
    });
    modal_view.show();
    //console.log(dataItem);
}
// ************************************* //
///////////////////////////////////////////
// ********        Action      *** ***** // 
$("#action").on("click", function () {
    var tipo_action = $("#tipo_action").val();
    switch (tipo_action) {
        case "create":
            action_crear();
            break;
        case "read":
            action_read();
            break;
        case "update":
            action_update();
            break;
        case "delete":
            action_delete();
            break;
        case "delete_all":
            action_delete_all();
            break;
    }
});
// ********   Action Create   *** ***** // 
function action_crear() {
    var empresa = {
        nit: $("#nit").val(),
        nombre: $("#nombre").val(),
        descripcion: $("#descripcion").val(),
        razonSocial: $("#razon_social").val(),
        emailPrincipal: $("#email_principal").val(),
        emailAlterno: $("#email_alterno").val(),
        telefonoPrincipal: $("#telefono_principal").val(),
        telefonoAlterno: $("#telefono_alterno").val(),
        sitioWeb: $("#sitio_web").val(),
        logo: $("#file_upload").val()
    };
    $.ajax({
        url: "api/empresa",
        type: "POST",
        dataType: "JSON",
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify(empresa),
        beforeSend: function (xhr) {
            //UIkit.modal.blockUI('<div class=\'uk-text-center\'>Guardando...<br/><img class=\'uk-margin-top\' src=\'/financy/resources/assets/img/spinners/spinner.gif\' alt=\'\'>');
        },
        success: function (data, textStatus, jqXHR) {
            modal_view.hide();
            $('#grid_empresa').data('kendoGrid').dataSource.read();
            UIkit.notify({
                message: "<a href='javascript:void(0);' class='notify-action'>X</a> Exito al guardar",
                status: 'success',
                timeout: 5000,
                pos: 'bottom-center'
            });
        },
        error: function (jqXHR, textStatus, errorThrown) {
            modal2("", "Error", "danger", "Aceptar");
            $("#modal-body2").html(jqXHR.responseText);
            modal_view2.show();
        }
    });

}
// ********    Action Read   *** ***** // 
function action_read() {
    alert("read");
}
// ********   Action Update  *** ***** // 
function action_update() {
    var empresa = {
        idEmpresa: parseInt($("#_id").val()),
        nit: $("#nit").val(),
        nombre: $("#nombre").val(),
        descripcion: $("#descripcion").val(),
        razonSocial: $("#razon_social").val(),
        emailPrincipal: $("#email_principal").val(),
        emailAlterno: $("#email_alterno").val(),
        telefonoPrincipal: $("#telefono_principal").val(),
        telefonoAlterno: $("#telefono_alterno").val(),
        sitioWeb: $("#sitio_web").val(),
        logo: $("#file_upload").val()
    };
    $.ajax({
        url: "api/empresa",
        type: "PUT",
        dataType: "JSON",
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify(empresa),
        beforeSend: function (xhr) {
            //UIkit.modal.blockUI('<div class=\'uk-text-center\'>Guardando...<br/><img class=\'uk-margin-top\' src=\'/financy/resources/assets/img/spinners/spinner.gif\' alt=\'\'>');
        },
        success: function (data, textStatus, jqXHR) {
            modal_view.hide();
            $('#grid_empresa').data('kendoGrid').dataSource.read();
            UIkit.notify({
                message: "<a href='javascript:void(0);' class='notify-action'>X</a> Exito al modificar",
                status: 'success',
                timeout: 5000,
                pos: 'bottom-center'
            });
        },
        error: function (jqXHR, textStatus, errorThrown) {
            modal2("", "Error", "danger", "Aceptar");
            $("#modal-body2").html(jqXHR.responseText);
            modal_view2.show();
        }
    });
}
// ********  Action Delete  *** ***** // 
function action_delete() {
    if (dataItem !== null) {
        console.log("dataItem", dataItem);
        $.ajax({
            url: "api/empresa/" + dataItem.id,
            type: "DELETE",
            beforeSend: function (xhr) {
                //UIkit.modal.blockUI('<div class=\'uk-text-center\'>Guardando...<br/><img class=\'uk-margin-top\' src=\'/financy/resources/assets/img/spinners/spinner.gif\' alt=\'\'>');
            },
            success: function (data, textStatus, jqXHR) {
                modal_view.hide();
                $('#grid_empresa').data('kendoGrid').dataSource.read();
                UIkit.notify({
                    message: "<a href='javascript:void(0);' class='notify-action'>X</a> Exito al eliminar",
                    status: 'success',
                    timeout: 5000,
                    pos: 'bottom-center'
                });
            },
            error: function (jqXHR, textStatus, errorThrown) {
                modal2("", "Error", "danger", "Aceptar");
                var error = "";
                if(jqXHR.responseText !== "") { error = jqXHR.responseText; }
                $("#modal-body2").html(error);
                modal_view2.show();
            }
        });
    }
}

// ********  Action Delete  *** ***** // 
function action_delete_all() {
    if (delVarAll !== null && delVarAll.length>0) {
        console.log("dataItem", delVarAll);
        $.ajax({
            url: "api/empresa",
            type: "DELETE",
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify(delVarAll),
            beforeSend: function (xhr) {
                //UIkit.modal.blockUI('<div class=\'uk-text-center\'>Guardando...<br/><img class=\'uk-margin-top\' src=\'/financy/resources/assets/img/spinners/spinner.gif\' alt=\'\'>');
            },
            success: function (data, textStatus, jqXHR) {
                modal_view.hide();
                $('#grid_empresa').data('kendoGrid').dataSource.read();
                UIkit.notify({
                    message: "<a href='javascript:void(0);' class='notify-action'>X</a> Exito al eliminar",
                    status: 'success',
                    timeout: 5000,
                    pos: 'bottom-center'
                });
            },
            error: function (jqXHR, textStatus, errorThrown) {
                modal2("", "Error", "danger", "Aceptar");
                var error = "";
                if(jqXHR.responseText !== "") { error = jqXHR.responseText; }
                $("#modal-body2").html(error);
                modal_view2.show();
            }
        });
    }
}

// ******** Cargar Datos Input ****** //
function cargar_input(item){
    for (var i = 1; i < 10; i++) {
        $("#d"+i).attr("class","md-input-wrapper md-input-filled");// abriendo los input para q no queden encaramaos
    }
    $("#_id").val(item.idEmpresa);
    $("#nit").val(item.nit);
    $("#nombre").val(item.nombre);
    $("#descripcion").val(item.descripcion);
    $("#razon_social").val(item.razonSocial);
    $("#email_principal").val(item.emailPrincipal);
    $("#email_alterno").val(item.emailAlterno);
    $("#telefono_principal").val(item.telefonoPrincipal);
    $("#telefono_alterno").val(item.telefonoAlterno);
    $("#sitio_web").val(item.sitioWeb);
    $("#file_upload").val(item.logo);
}