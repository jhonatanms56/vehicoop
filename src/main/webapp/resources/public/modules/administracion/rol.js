/* global kendo, UIkit, modal_view, modal_view2 */

// ******** Data Sources  (Read) ********* //
var dataSource = new kendo.data.DataSource({
    transport: {
        read: {
            url: "api/rol",
            contentType: "application/json; charset=utf-8",
            type: "GET",
            async: false
        }
    },
    pageSize: 10,
    schema: {
        model: {
            id: "idRol",
            fields: {
                idRol: {type: "number"},
                nombre: {type: "string"},
                descripcion: {type: "string"}                
            }
        }
    }
});
// *** **** Data Sources End *** ***** //
console.log("data_sources", dataSource);
// ********        Grid      ********* //
var grid_rol = $("#grid_rol").kendoGrid({
    toolbar: [{template: kendo.template($("#template").html())}],
    //detailTemplate: kendo.template($("#detail-template").html()),
    columns: [
        {field: "nombre", title: "Nombre"},
        {field: "descripcion", title: "Descripción"},
        {
            command:
            [
                {
                    name: "Editar",
                    text: "",
                    imageClass: "uk-icon-edit",
                    title: "Editar",
                    click: editar
                },
                {
                    name: "Eliminar",
                    text: "",
                    imageClass: "uk-icon-trash",
                    title: "Eliminar",
                    click: eliminar
                }
            ],
            title: "&nbsp;"
        }
    ],
    dataSource: dataSource,
    selectable: "multiple row",
    sortable: true,
    reorderable: true,
    groupable: true,
    resizable: true,
    filterable: true,
    columnMenu: true,
    pageable: {
        pageSize: 10,
        pageSizes: true,
        refresh: true
    }
});
// *** ****      Grid End    *** ***** //
var dataItem = null;
// ********       Create     *** *****// 
function crear() {
    modal("create", "Crear", "primary", "Guardar");
    $.ajax({
        url: "rol/create",
        beforeSend: function (xhr) {

        },
        success: function (data, textStatus, jqXHR) {
            $("#modal-body").html(data);
            permisos();
        },
        error: function (jqXHR, textStatus, errorThrown) {

        }
    });
    modal_view.show();
}
// ********        Delete    *** ***** // 
function eliminar(e) {
    e.preventDefault();
    dataItem = this.dataItem($(e.currentTarget).closest("tr"));
    modal("delete", "Eliminar", "danger", "Aceptar");
    $("#modal-body").html("<h4>Realmente desea eliminar la rol seleccionada?</h4>");
    modal_view.show();
}
// ********     Delete All     *** ***** //
var delVarAll = new Array();
function deleteAll() {
    var grid = $('#grid_rol').data('kendoGrid');
    //console.log("tamaño: ", grid.select().length);
    if(grid.select().length>0){
        grid.select().each(function () {
            var dataItem = grid.dataItem($(this));
            delVarAll.push(dataItem.idEmpresa);
        });
        //console.log("id",delVarAll);
        //console.log("grid_rol: ",grid.select());
        modal("delete_all", "Eliminar Selección", "danger", "Aceptar");
        $("#modal-body").html("<h4>Realmente desea eliminar las rols seleccionadas?</h4>");
        modal_view.show();
    } else {
        UIkit.notify({
            message: "<a href='javascript:void(0);' class='notify-action'>X</a> Seleccione por lo menos una fila",
            status: 'warning',
            timeout: 5000,
            pos: 'bottom-center'
        });
    }
    
}
// ********        Update      *** ***** // 
function editar(e) {
    e.preventDefault();
    dataItem = this.dataItem($(e.currentTarget).closest("tr"));
    modal("update", "Actualizar", "primary", "Guardar");
    $.ajax({
        url: "rol/create",
        beforeSend: function (xhr) {

        },
        success: function (data, textStatus, jqXHR) {
            $("#modal-body").html(data);
            cargar_input(dataItem);
            permisos();
        },
        error: function (jqXHR, textStatus, errorThrown) {

        }
    });
    modal_view.show();
    //console.log(dataItem);
}
// ************************************* //
///////////////////////////////////////////
// ********        Action      *** ***** // 
$("#action").on("click", function () {
    var tipo_action = $("#tipo_action").val();
    switch (tipo_action) {
        case "create":
            action_crear();
            break;
        case "read":
            action_read();
            break;
        case "update":
            action_update();
            break;
        case "delete":
            action_delete();
            break;
        case "delete_all":
            action_delete_all();
            break;
    }
});
// ********   Action Create   *** ***** // 
function action_crear() {
    var rol = {
        nombre: 'ROLE_'+$("#nombre").val().toString().toUpperCase().replace(" ","_"),
        descripcion: $("#descripcion").val()
    };
    $.ajax({
        url: "api/rol",
        type: "POST",
        dataType: "JSON",
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify(rol),
        beforeSend: function (xhr) {
            //UIkit.modal.blockUI('<div class=\'uk-text-center\'>Guardando...<br/><img class=\'uk-margin-top\' src=\'/financy/resources/assets/img/spinners/spinner.gif\' alt=\'\'>');
        },
        success: function (data, textStatus, jqXHR) {
            modal_view.hide();
            $('#grid_rol').data('kendoGrid').dataSource.read();
            UIkit.notify({
                message: "<a href='javascript:void(0);' class='notify-action'>X</a> Exito al guardar",
                status: 'success',
                timeout: 5000,
                pos: 'bottom-center'
            });
        },
        error: function (jqXHR, textStatus, errorThrown) {
            modal2("", "Error", "danger", "Aceptar");
            $("#modal-body2").html(jqXHR.responseText);
            modal_view2.show();
        }
    });

}
// ********    Action Read   *** ***** // 
function action_read() {
    alert("read");
}
// ********   Action Update  *** ***** // 
function action_update() {
    var rol = {
        idEmpresa: parseInt($("#_id").val()),
        nit: $("#nit").val(),
        nombre: $("#nombre").val(),
        descripcion: $("#descripcion").val(),
        razonSocial: $("#razon_social").val(),
        emailPrincipal: $("#email_principal").val(),
        emailAlterno: $("#email_alterno").val(),
        telefonoPrincipal: $("#telefono_principal").val(),
        telefonoAlterno: $("#telefono_alterno").val(),
        sitioWeb: $("#sitio_web").val(),
        logo: $("#file_upload").val()
    };
    $.ajax({
        url: "api/rol",
        type: "PUT",
        dataType: "JSON",
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify(rol),
        beforeSend: function (xhr) {
            //UIkit.modal.blockUI('<div class=\'uk-text-center\'>Guardando...<br/><img class=\'uk-margin-top\' src=\'/financy/resources/assets/img/spinners/spinner.gif\' alt=\'\'>');
        },
        success: function (data, textStatus, jqXHR) {
            modal_view.hide();
            $('#grid_rol').data('kendoGrid').dataSource.read();
            UIkit.notify({
                message: "<a href='javascript:void(0);' class='notify-action'>X</a> Exito al modificar",
                status: 'success',
                timeout: 5000,
                pos: 'bottom-center'
            });
        },
        error: function (jqXHR, textStatus, errorThrown) {
            modal2("", "Error", "danger", "Aceptar");
            $("#modal-body2").html(jqXHR.responseText);
            modal_view2.show();
        }
    });
}
// ********  Action Delete  *** ***** // 
function action_delete() {
    if (dataItem !== null) {
        $.ajax({
            url: "api/rol/" + dataItem.id,
            type: "DELETE",
            beforeSend: function (xhr) {
                //UIkit.modal.blockUI('<div class=\'uk-text-center\'>Guardando...<br/><img class=\'uk-margin-top\' src=\'/financy/resources/assets/img/spinners/spinner.gif\' alt=\'\'>');
            },
            success: function (data, textStatus, jqXHR) {
                modal_view.hide();
                $('#grid_rol').data('kendoGrid').dataSource.read();
                UIkit.notify({
                    message: "<a href='javascript:void(0);' class='notify-action'>X</a> Exito al eliminar",
                    status: 'success',
                    timeout: 5000,
                    pos: 'bottom-center'
                });
            },
            error: function (jqXHR, textStatus, errorThrown) {
                modal2("", "Error", "danger", "Aceptar");
                var error = "";
                if(jqXHR.responseText !== "") { error = jqXHR.responseText; }
                $("#modal-body2").html(error);
                modal_view2.show();
            }
        });
    }
}

// ********  Action Delete All  *** ***** // 
function action_delete_all() {
    if (delVarAll !== null && delVarAll.length>0) {
        $.ajax({
            url: "api/rol",
            type: "DELETE",
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify(delVarAll),
            beforeSend: function (xhr) {
                //UIkit.modal.blockUI('<div class=\'uk-text-center\'>Guardando...<br/><img class=\'uk-margin-top\' src=\'/financy/resources/assets/img/spinners/spinner.gif\' alt=\'\'>');
            },
            success: function (data, textStatus, jqXHR) {
                modal_view.hide();
                $('#grid_rol').data('kendoGrid').dataSource.read();
                UIkit.notify({
                    message: "<a href='javascript:void(0);' class='notify-action'>X</a> Exito al eliminar",
                    status: 'success',
                    timeout: 5000,
                    pos: 'bottom-center'
                });
            },
            error: function (jqXHR, textStatus, errorThrown) {
                modal2("", "Error", "danger", "Aceptar");
                var error = "";
                if(jqXHR.responseText !== "") { error = jqXHR.responseText; }
                $("#modal-body2").html(error);
                modal_view2.show();
            }
        });
    }
}

// ******** Cargar Datos Input ****** //
function cargar_input(item){
    console.log(item);
    for (var i = 1; i < 10; i++) {
        $("#d"+i).attr("class","md-input-wrapper md-input-filled");// abriendo los input para q no queden encaramaos
    }
    $("#_id").val(item.idPermiso);
    $("#nombre").val(item.nombre);
    $("#descripcion").val(item.descripcion);
}

// Permisos ///
function permisos(){
    $.ajax({
        url: "api/permiso",
        beforeSend: function (xhr) {
            console.log("cargando...");
            $('#check_permiso').html('<span>Cargando...</span>');
        },
        success: function (data, textStatus, jqXHR) {
            var check_line = ''; 
            $.each(data,function(index,item){
                check_line += '<p>'+
                                '<input type="checkbox" name="p_'+item.idPermiso+'"'
                                +'id="p_'+item.idPermiso+'" data-md-icheck />'
                                +'<label title="'+item.descripcion+'"'
                                +'for="p_'+item.idPermiso+'" class="inline-label">'
                                +item.nombre+'</label></p>';
            });
            $('#check_permiso').html(check_line);
        },
        error: function (jqXHR, textStatus, errorThrown) {

        }
    });
}