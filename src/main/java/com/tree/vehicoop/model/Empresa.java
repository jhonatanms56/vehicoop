/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tree.vehicoop.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Mr. Robot
 */
@Entity
@Table(name = "empresa", uniqueConstraints = {
    @UniqueConstraint(columnNames = {"email_principal"})
    , @UniqueConstraint(columnNames = {"nit"})})
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Empresa.findAll", query = "SELECT e FROM Empresa e")
    , @NamedQuery(name = "Empresa.findByIdEmpresa", query = "SELECT e FROM Empresa e WHERE e.idEmpresa = :idEmpresa")
    , @NamedQuery(name = "Empresa.findByNit", query = "SELECT e FROM Empresa e WHERE e.nit = :nit")
    , @NamedQuery(name = "Empresa.findByNombre", query = "SELECT e FROM Empresa e WHERE e.nombre = :nombre")
    , @NamedQuery(name = "Empresa.findByDescripcion", query = "SELECT e FROM Empresa e WHERE e.descripcion = :descripcion")
    , @NamedQuery(name = "Empresa.findByRazonSocial", query = "SELECT e FROM Empresa e WHERE e.razonSocial = :razonSocial")
    , @NamedQuery(name = "Empresa.findByEmailPrincipal", query = "SELECT e FROM Empresa e WHERE e.emailPrincipal = :emailPrincipal")
    , @NamedQuery(name = "Empresa.findByEmailAlterno", query = "SELECT e FROM Empresa e WHERE e.emailAlterno = :emailAlterno")
    , @NamedQuery(name = "Empresa.findByTelefonoPrincipal", query = "SELECT e FROM Empresa e WHERE e.telefonoPrincipal = :telefonoPrincipal")
    , @NamedQuery(name = "Empresa.findByTelefonoAlterno", query = "SELECT e FROM Empresa e WHERE e.telefonoAlterno = :telefonoAlterno")
    , @NamedQuery(name = "Empresa.findBySitioWeb", query = "SELECT e FROM Empresa e WHERE e.sitioWeb = :sitioWeb")
    , @NamedQuery(name = "Empresa.findByLogo", query = "SELECT e FROM Empresa e WHERE e.logo = :logo")})
public class Empresa implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_empresa", nullable = false)
    private Long idEmpresa;
    @Basic(optional = false)
    @Column(name = "nit", nullable = false, length = 50)
    private String nit;
    @Basic(optional = false)
    @Column(name = "nombre", nullable = false, length = 100)
    private String nombre;
    @Column(name = "descripcion", length = 500)
    private String descripcion;
    @Basic(optional = false)
    @Column(name = "razon_social", nullable = false, length = 1000)
    private String razonSocial;
    @Basic(optional = false)
    @Column(name = "email_principal", nullable = false, length = 50)
    private String emailPrincipal;
    @Column(name = "email_alterno", length = 50)
    private String emailAlterno;
    @Basic(optional = false)
    @Column(name = "telefono_principal", nullable = false, length = 20)
    private String telefonoPrincipal;
    @Column(name = "telefono_alterno", length = 20)
    private String telefonoAlterno;
    @Column(name = "sitio_web", length = 200)
    private String sitioWeb;
    @Column(name = "logo", length = 1000)
    private String logo;
    @JoinColumn(name = "id_licencia", referencedColumnName = "id_licencia")
    @ManyToOne
    private Licencia idLicencia;

    public Empresa() {
    }

    public Empresa(Long idEmpresa) {
        this.idEmpresa = idEmpresa;
    }

    public Empresa(Long idEmpresa, String nit, String nombre, String razonSocial, String emailPrincipal, String telefonoPrincipal) {
        this.idEmpresa = idEmpresa;
        this.nit = nit;
        this.nombre = nombre;
        this.razonSocial = razonSocial;
        this.emailPrincipal = emailPrincipal;
        this.telefonoPrincipal = telefonoPrincipal;
    }

    public Long getIdEmpresa() {
        return idEmpresa;
    }

    public void setIdEmpresa(Long idEmpresa) {
        this.idEmpresa = idEmpresa;
    }

    public String getNit() {
        return nit;
    }

    public void setNit(String nit) {
        this.nit = nit;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getRazonSocial() {
        return razonSocial;
    }

    public void setRazonSocial(String razonSocial) {
        this.razonSocial = razonSocial;
    }

    public String getEmailPrincipal() {
        return emailPrincipal;
    }

    public void setEmailPrincipal(String emailPrincipal) {
        this.emailPrincipal = emailPrincipal;
    }

    public String getEmailAlterno() {
        return emailAlterno;
    }

    public void setEmailAlterno(String emailAlterno) {
        this.emailAlterno = emailAlterno;
    }

    public String getTelefonoPrincipal() {
        return telefonoPrincipal;
    }

    public void setTelefonoPrincipal(String telefonoPrincipal) {
        this.telefonoPrincipal = telefonoPrincipal;
    }

    public String getTelefonoAlterno() {
        return telefonoAlterno;
    }

    public void setTelefonoAlterno(String telefonoAlterno) {
        this.telefonoAlterno = telefonoAlterno;
    }

    public String getSitioWeb() {
        return sitioWeb;
    }

    public void setSitioWeb(String sitioWeb) {
        this.sitioWeb = sitioWeb;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public Licencia getIdLicencia() {
        return idLicencia;
    }

    public void setIdLicencia(Licencia idLicencia) {
        this.idLicencia = idLicencia;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idEmpresa != null ? idEmpresa.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Empresa)) {
            return false;
        }
        Empresa other = (Empresa) object;
        if ((this.idEmpresa == null && other.idEmpresa != null) || (this.idEmpresa != null && !this.idEmpresa.equals(other.idEmpresa))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entitys.Empresa[ idEmpresa=" + idEmpresa + " ]";
    }

}
