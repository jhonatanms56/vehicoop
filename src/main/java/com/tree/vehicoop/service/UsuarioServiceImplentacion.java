/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tree.vehicoop.service;

import com.tree.vehicoop.model.Usuario;
import com.tree.vehicoop.repository.UsuarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
/**
 *
 * @author Mr. Robot
 */
@Service
public class UsuarioServiceImplentacion implements UsuarioService{

    @Autowired
    private UsuarioRepository ur;
    
    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;
    
    @Override
    public Usuario findByUsername(String username) {
        return ur.findByUsername(username);
    }

}
