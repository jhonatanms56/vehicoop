/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tree.vehicoop.service;

import com.tree.vehicoop.model.Usuario;

/**
 *
 * @author Mr. Robot
 */
public interface UsuarioService {
    Usuario findByUsername(String username);
}
