/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tree.vehicoop.service;

import com.tree.vehicoop.model.Rol;
import com.tree.vehicoop.model.Usuario;
import com.tree.vehicoop.repository.UsuarioRepository;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

/**
 *
 * @author Mr. Robot
 */
@Service
@Scope(proxyMode = ScopedProxyMode.TARGET_CLASS)
public class AccountService implements UserDetailsService {

    @Autowired
    private UsuarioRepository usuarioRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Usuario usuario = usuarioRepository.findByUsername(username);
        if (usuario == null) {
            throw new UsernameNotFoundException("user not found");
        }
        return createUser(usuario);
    }

    public void signin(Usuario account) {
        SecurityContextHolder.getContext().setAuthentication(authenticate(account));
    }

    private Authentication authenticate(Usuario account) {
        return new UsernamePasswordAuthenticationToken(createUser(account), null, createAuthority(account));
    }

    private User createUser(Usuario account) {
        return new User(account.getEmail(), account.getPassword(), createAuthority(account));
    }

    private List<GrantedAuthority> createAuthority(Usuario usuario) {
        List<GrantedAuthority> authorities = new ArrayList<>();
        for (Rol rol : usuario.getRolList()) {
            authorities.add(new SimpleGrantedAuthority(rol.getNombre()));
        }
        return authorities;
    }
}
