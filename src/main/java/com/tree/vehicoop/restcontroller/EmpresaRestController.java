/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tree.vehicoop.restcontroller;

import com.tree.vehicoop.model.Empresa;
import com.tree.vehicoop.repository.EmpresaRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Mr. Robot
 */
@RestController
public class EmpresaRestController {
    
    @Autowired
    EmpresaRepository empresaRepository;
    
    @GetMapping(value = {"/api/empresa"}, produces=MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Empresa>> findAll() throws Exception{
        List<Empresa> empresas = empresaRepository.findAll();
        return new ResponseEntity<>(empresas, HttpStatus.OK);
    }
    
    @PostMapping(value = {"/api/empresa"}, produces=MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Empresa> create(@RequestBody Empresa empresa) throws Exception{
        if(empresa!=null){
            empresa = empresaRepository.save(empresa);
            return new ResponseEntity<>(empresa, HttpStatus.CREATED);
        }
        return new ResponseEntity<>(empresa, HttpStatus.BAD_REQUEST);
    }
    
    @DeleteMapping(value = {"/api/empresa"}, produces=MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Void> deleteAll(@RequestBody List<Long> ids) throws Exception{
        System.out.println(ids);
        if(ids!=null){
            System.out.println("Entre.. a delete all");
            empresaRepository.deleteAllSelects(ids);
            return new ResponseEntity<Void>(HttpStatus.OK);
        }
        return new ResponseEntity<Void>(HttpStatus.BAD_REQUEST);
    }
    
    // DELETE PERSONA BY ID
    @DeleteMapping(value="/api/empresa/{id}")
    public ResponseEntity<Void> delete(@PathVariable Long id) throws Exception{
        if(id != null){
            empresaRepository.delete(id);
            return new ResponseEntity<Void>(HttpStatus.OK);
        }
        return new ResponseEntity<Void>(HttpStatus.BAD_REQUEST);
    }
    
    @PutMapping(value = {"/api/empresa"}, produces=MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Empresa> update(@RequestBody Empresa empresa) throws Exception{
        if(empresa!=null){
            empresa = empresaRepository.save(empresa);
            return new ResponseEntity<>(empresa, HttpStatus.OK);
        }
        return new ResponseEntity<>(empresa, HttpStatus.BAD_REQUEST);
    }
}
