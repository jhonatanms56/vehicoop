/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tree.vehicoop.restcontroller;

import com.tree.vehicoop.model.Rol;
import com.tree.vehicoop.repository.RolRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Mr. Robot
 */
@RestController
public class RolRestController {
    
    @Autowired
    RolRepository rolRepository;
    
    @GetMapping(value = {"/api/rol"}, produces=MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Rol>> findAll() throws Exception{
        List<Rol> rols = rolRepository.findAll();
        return new ResponseEntity<>(rols, HttpStatus.OK);
    }
    
    @GetMapping(value = {"/api/rol/{id}"}, produces=MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Rol> findOne(@PathVariable Long id) throws Exception{
        Rol rol = rolRepository.findOne(id);
        return new ResponseEntity<>(rol, HttpStatus.OK);
    }
    
    @PostMapping(value = {"/api/rol"}, produces=MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Rol> create(@RequestBody Rol rol) throws Exception{
        if(rol!=null){
            rol = rolRepository.save(rol);
            return new ResponseEntity<>(rol, HttpStatus.CREATED);
        }
        return new ResponseEntity<>(rol, HttpStatus.BAD_REQUEST);
    }
    
//    @DeleteMapping(value = {"/api/rol"}, produces=MediaType.APPLICATION_JSON_VALUE)
//    public ResponseEntity<Void> deleteAll(@RequestBody List<Long> ids) throws Exception{
//        System.out.println(ids);
//        if(ids!=null){
//            System.out.println("Entre.. a delete all");
//            rolRepository.deleteAllSelects(ids);
//            return new ResponseEntity<Void>(HttpStatus.OK);
//        }
//        return new ResponseEntity<Void>(HttpStatus.BAD_REQUEST);
//    }
    
    // DELETE PERSONA BY ID
    @DeleteMapping(value="/api/rol/{id}")
    public ResponseEntity<Void> delete(@PathVariable Long id) throws Exception{
        if(id != null){
            rolRepository.delete(id);
            return new ResponseEntity<Void>(HttpStatus.OK);
        }
        return new ResponseEntity<Void>(HttpStatus.BAD_REQUEST);
    }
    
    @PutMapping(value = {"/api/rol"}, produces=MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Rol> update(@RequestBody Rol rol) throws Exception{
        if(rol!=null){
            rol = rolRepository.save(rol);
            return new ResponseEntity<>(rol, HttpStatus.OK);
        }
        return new ResponseEntity<>(rol, HttpStatus.BAD_REQUEST);
    }
}
