/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tree.vehicoop.util;

import java.util.Collection;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import org.springframework.stereotype.Component;

/**
 *
 * @author LuiFer
 */
@Component
public class ReportingUtil {

    public JRDataSource convertToDataSource(Collection<?> collection) {
        JRDataSource ds = new JRBeanCollectionDataSource(collection, false);
        // Return the wrapped collection
        return ds;
    }
}
