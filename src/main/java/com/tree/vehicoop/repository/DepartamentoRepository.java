/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tree.vehicoop.repository;

import com.tree.vehicoop.model.Departamento;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author Mr. Robot
 */
public interface DepartamentoRepository extends JpaRepository<Departamento, Long>{

}
