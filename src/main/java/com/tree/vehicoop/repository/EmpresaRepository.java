/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tree.vehicoop.repository;

import com.tree.vehicoop.model.Empresa;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Mr. Robot
 */
public interface EmpresaRepository extends JpaRepository<Empresa, Long>{
    @Transactional
    @Modifying
    @Query("DELETE FROM Empresa e WHERE e.idEmpresa IN ?1")
    void deleteAllSelects(List<Long> ids);
}
