/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tree.vehicoop.controller;


import com.tree.vehicoop.util.ReportingUtil;
import org.springframework.beans.factory.annotation.Autowired;

import com.tree.vehicoop.util.HibernateQueryResultDataSource;


import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import javax.servlet.http.HttpServletRequest;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author 
 */
@Controller
public class ReportesController {

    @Autowired
    ReportingUtil reportingUtil;

    


  //  @Autowired
  //  TranslateUtil translate;


    public void setReportingUtil(ReportingUtil reportingUtil) {
        this.reportingUtil = reportingUtil;
    }
    
    @Transactional
    @RequestMapping(value = "/reporte/rptpresupuestoingresoini", method = RequestMethod.GET)
    public ModelAndView rptordenservicio(@RequestParam("presupuestoId") long presupuestoId, ModelAndView modelAndView, ModelMap model, @RequestParam("type") String type, HttpServletRequest request) {

        // Assign the datasource to an instance of JRDataSource
        // JRDataSource is the datasource that Jasper understands
        // This is basically a wrapper to Java's collection classes
        
        //PInicialIngreso pi = pinicialingresoRepository.findOne(presupuestoId);
        List data = new ArrayList();//pinicialingresoRepository.findByPresupuestoId(presupuestoId);
        
        String[] fields = new String[]{"id", "documento", "nombre", "descripcion"};
        HibernateQueryResultDataSource ds = new HibernateQueryResultDataSource(data, fields);
        
        /*List data = new ArrayList();
        Object[] obj = new Object[] {pi.getId(),pi.getDocumento(),pi.getNombre(),pi.getDescripcion()};
        data.add(obj);

        String[] fields = new String[]{"documento", "nombre"};
        HibernateQueryResultDataSource ds = new HibernateQueryResultDataSource(data, fields);
        */
        // In order to use Spring's built-in Jasper support,
        // We are required to pass our datasource as a map parameter
        // Add our datasource parameter

        model.addAttribute("datasource", ds);
        //  model.
        // Add the report format
        model.addAttribute("format", type);


        model.put("type", type);
        //model.put("documento", translate.localizeMessage("reporte.presupuestoinicialingreso.documento"));
        //model.put("nombre", translate.localizeMessage("reporte.presupuestoinicialingreso.nombre"));
        model.put("documento", "Documento");
        model.put("nombre", "Nombre");
        model.put("type", type);


        model.put("host", request.getScheme() + "://" + request.getServerName() + ":" + request.getLocalPort() + request.getContextPath());

        // multiReport is the View of our application
        // This is declared inside the /WEB-INF/jasper-views.xml
        modelAndView = new ModelAndView("rpt_presupuestoingreso", model);

        // Return the View and the Model combined
        return modelAndView;
    }

   

}
