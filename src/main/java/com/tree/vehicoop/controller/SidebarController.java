/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tree.vehicoop.controller;

import com.tree.vehicoop.model.Opcion;
import com.tree.vehicoop.repository.OpcionRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ModelAttribute;

/**
 *
 * @author Mr. Robot
 */
@ControllerAdvice
public class SidebarController {
    
    @Autowired
    OpcionRepository opcionRepository;
    
    @ModelAttribute("opciones")
    public List<Opcion> menu(){
        List<Opcion> opciones = opcionRepository.findAll();
        return opciones;
    }
    
}
