/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tree.vehicoop.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author Mr. Robot
 */
@Controller
public class EmpresaController {

    private final String VIEW = "pages/modules/configuracion/empresa/";
    
    @RequestMapping(value = {"/empresa"}, method = RequestMethod.GET)
    public String empresa_read (){
        return VIEW+"empresa";
    }
    
    @RequestMapping(value = {"/empresa/create"}, method = RequestMethod.GET)
    public String empresa_create (){
        return VIEW+"create";
    }
    
    @RequestMapping(value = {"/empresa/update"}, method = RequestMethod.GET)
    public String empresa_update (){
        return VIEW+"create";
    }
    
    @RequestMapping(value = {"/empresa/delete"}, method = RequestMethod.GET)
    public String empresa_delete (){
        return VIEW+"create";
    }
}
