/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tree.vehicoop.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author Mr. Robot
 */
@Controller
public class HomeController {

    @RequestMapping(value = {"/home", "/dashboard"}, method = RequestMethod.GET)
    public String home(){
        return "pages/home/home";
    }
    
    @RequestMapping(value = {"/blank"}, method = RequestMethod.GET)
    public String usuario(){
        return "pages/blank_page/index";
    }
    
     
    
}
