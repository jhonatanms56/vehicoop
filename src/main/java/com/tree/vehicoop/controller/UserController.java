/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tree.vehicoop.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author Win 10
 */

@Controller
public class UserController {
    
     @RequestMapping(value = {"/userconf"}, method = RequestMethod.GET)
    public String codpregastos(){
        return "pages/user/profile/page_user_profile";
    }
    
}
