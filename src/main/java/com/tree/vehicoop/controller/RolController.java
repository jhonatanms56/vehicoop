/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tree.vehicoop.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author Mr. Robot
 */
@Controller
public class RolController {
    
    private final String VIEW = "pages/modules/administracion/rol/";
    
    @RequestMapping(value = {"/rol"}, method = RequestMethod.GET)
    public String rol_read(){
        return VIEW+"rol";
    }
    
    @RequestMapping(value = {"/rol/create"}, method = RequestMethod.GET)
    public String empresa_create (){
        return VIEW+"create";
    }
    
}
